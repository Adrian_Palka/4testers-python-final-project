class Order:
    def __init__(self, customer_email):
        self.customer_email = customer_email
        self.products = []
        self.purchased = False

    def add_product(self, product):
        self.products.append(product)

    def get_total_price(self):
        sum = 0
        for product in self.products:
            sum += product.get_price()
        return sum

    def get_total_quantity_of_products(self):
        sum = 0
        for product in self.products:
            sum += product.quantity
        return sum

    def purchase(self):
        self.purchased = True


class Product:
    def __init__(self, name, unit_price, quantity=1):
        self.name = name
        self.unit_price = unit_price
        self.quantity = quantity

    def get_price(self):
        return self.unit_price * self.quantity


if __name__ == '__main__':
    product1 = Product("plate", 5, 2)
    print(product1.unit_price)
    print(product1.get_price())

    order1 = Order("dj@trololo.com")
    print(order1.products)

    product2 = "trololo"
    order1.add_product(product1)
    print(len(order1.products))
